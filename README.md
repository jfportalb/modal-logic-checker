# Modal Logic Checker
![Build Status](https://gitlab.com/jfportalb/modal-logic-checker/badges/master/build.svg)

---

## How to run it

The program is available [here](https://jfportalb.gitlab.io/modal-logic-checker/) 

If your browser doesn't support it, clone the project and execute the following comands to run it locally with nodejs:
```sh
npm install
./node_modules/.bin/tsc
nodejs public/app.js
```