import {Model, State} from "./model";

export enum FormulaType {
    Atom,
    Not,     // ¬
    And,     // ∧
    Or,      // ∨
    Implies, // →
    ForAll,  // □
    Exists,  // ◇
    Knows,   // K
    Believes,// B
    PublicAnnouncement // []
}

export class Formula {
    private type: FormulaType;
    private f1: Formula;
    private f2: Formula;
    private agent: string;

    constructor(f1: Formula | string, type?: FormulaType, f2?: Formula | string, s?: string) {
        if (f1 instanceof Formula) {
            this.f1 = f1;
            this.type = type;
            if (f2 instanceof Formula)
                this.f2 = f2;
            else if (type == FormulaType.ForAll || type == FormulaType.Exists || type == FormulaType.Knows || type == FormulaType.Believes)
                this.agent = f2;
            if (s) this._s = s;
        } else {
            this._s = f1;
            this.type = FormulaType.Atom;
        }
    }

    private _s: string;
    public toString(): string {
        if (this._s == undefined)
            switch (this.type) {
                case FormulaType.Not:
                    this._s =  `¬${this.f1.toString()}`;
                    break;
                case FormulaType.And:
                    this._s =  `(${this.f1.toString()}∧${this.f2.toString()})`;
                    break;
                case FormulaType.Or:
                    this._s =  `(${this.f1.toString()}∨${this.f2.toString()})`;
                    break;
                case FormulaType.Implies:
                    this._s =  `(${this.f1.toString()}→${this.f2.toString()})`;
                    break;
                case FormulaType.ForAll:
                    this._s =  `□${this.agent} ${this.f1.toString()}`;
                    break;
                case FormulaType.Exists:
                    this._s =  `◇${this.agent} ${this.f1.toString()}`;
                    break;
                case FormulaType.Knows:
                    this._s =  `K${this.agent} ${this.f1.toString()}`;
                    break;
                case FormulaType.Believes:
                    this._s =  `B${this.agent} ${this.f1.toString()}`;
                    break;
                case FormulaType.PublicAnnouncement:
                    this._s =  `[${this.f1.toString()}]${this.f2.toString()})`;
                    break;
                default:
                    this._s =  "⊥";
                    break;
            }
        return this._s;
    }

    public evaluate(m: Model, w: State): boolean {
        let r: boolean = m.V(w, this.toString());
        if (r != undefined) return r;
        let n: State[];
        switch (this.type) {
            case FormulaType.Atom:
                r = false;
                break;
            case FormulaType.Not:
                r = !this.f1.evaluate(m, w);
                break;
            case FormulaType.And:
                r = this.f1.evaluate(m, w) && this.f2.evaluate(m, w);
                break;
            case FormulaType.Or:
                r = this.f1.evaluate(m, w) || this.f2.evaluate(m, w);
                break;
            case FormulaType.Implies:
                r = !this.f1.evaluate(m, w) || this.f2.evaluate(m, w);
                break;
            case FormulaType.ForAll:
            case FormulaType.Knows:
                r = true;
                n = m.getNeighbours(w, this.agent);
                for (let i = 0; i < n.length; i++) {
                    if (!this.f1.evaluate(m, n[i])) {
                        r = false;
                        break;
                    }
                }
                break;
            case FormulaType.Exists:
            case FormulaType.Believes:
                r = false;
                n = m.getNeighbours(w, this.agent);
                for (let i = 0; i < n.length; i++) {
                    if (this.f1.evaluate(m, n[i])) {
                        r = true;
                        break;
                    }
                }
                break;
            case FormulaType.PublicAnnouncement:
                if (!this.f1.evaluate(m, w)){
                    r = true;
                    break;
                }
                let updatedModel = m.transformAfterPublicAnnouncement(this.f1);
                console.log(updatedModel);
                r = this.f2.evaluate(updatedModel, w);
                break;
        }
        m.addV(w, this.toString(), r);
        return r;
    }

    private static parseMiddle(formulaString: string, begin: number): [Formula, number] {
        let f, end, ff, a, f1, f2;
        switch (formulaString[begin]) {
            case '(':
                f1 = this.parseMiddle(formulaString, begin+1);
                let type;
                switch (formulaString[f1[1]]) {
                    case '∨':
                        type = FormulaType.Or;
                        break;
                    case '→':
                        type = FormulaType.Implies;
                        break;
                    case '∧':
                    default:
                        type = FormulaType.And;
                }
                f2 = this.parseMiddle(formulaString, f1[1]+1);
                end = f2[1]+1; // Skip ')'
                f = new Formula(f1[0], type, f2[0], formulaString.substring(begin, end));
                break;
            case '[':
                f1 = this.parseMiddle(formulaString, begin+1);
                // f1[1] == ']'
                f2 = this.parseMiddle(formulaString, f1[1]+1);
                f = new Formula(f1[0], FormulaType.PublicAnnouncement, f2[0], formulaString.substring(begin, end));
                break;
            case '¬':
                ff = this.parseMiddle(formulaString, begin+1);
                end = ff[1];
                f = new Formula(ff[0], FormulaType.Not, null, formulaString.substring(begin, end));
                break;
            case '□':
                a = this.parseAgent(formulaString, begin+1);
                ff = this.parseMiddle(formulaString, a[1]+1);
                end = ff[1];
                f = new Formula(ff[0], FormulaType.ForAll, a[0], formulaString.substring(begin, end));
                break;
            case '◇':
                a = this.parseAgent(formulaString, begin+1);
                ff = this.parseMiddle(formulaString, a[1]+1);
                end = ff[1];
                f = new Formula(ff[0], FormulaType.Exists, a[0], formulaString.substring(begin, end));
                break;
            case 'K':
                a = this.parseAgent(formulaString, begin+1);
                ff = this.parseMiddle(formulaString, a[1]+1);
                end = ff[1];
                f = new Formula(ff[0], FormulaType.Knows, a[0], formulaString.substring(begin, end));
                break;
            case 'B':
                a = this.parseAgent(formulaString, begin+1);
                ff = this.parseMiddle(formulaString, a[1]+1);
                end = ff[1];
                f = new Formula(ff[0], FormulaType.Believes, a[0], formulaString.substring(begin, end));
                break;
            default:
                end = begin+1;
                while (formulaString[end]!=undefined && formulaString[end].toLowerCase() != formulaString[end].toUpperCase()) end++;
                f = new Formula(formulaString.substring(begin, end));
        }
        return [f, end]
    }

    static parse(formulaString: string): Formula {
        return this.parseMiddle(formulaString, 0)[0];
    }

    private static parseAgent(formulaString: string, begin: number): [string, number] {
        let end = begin+1;
        while (formulaString[end]!=undefined && formulaString[end].toLowerCase() != formulaString[end].toUpperCase()) end++;
        return [formulaString.substring(begin, end), end];
    }
}