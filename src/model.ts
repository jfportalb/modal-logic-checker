import {Formula} from "./formula";

export type State = string;
export class Model {
    // noinspection JSMismatchedCollectionQueryUpdate
    private readonly _W: State[]; // Variável presente por motivos de formalidade. Para o escopo do problema, vamos assumir que quaisquer estados passados nas funções pertencem ao modelo.
    private readonly _R: State[/*w1*/][/*a*/][];
    private readonly _baseV: boolean[/*w1*/][/*p*/];
    private readonly _V: boolean[/*w1*/][/*p*/];

    constructor(W: State[], R: State[][][], V: boolean[][]) {
        this._W = W;
        this._R = R;
        this._baseV = V;
        this._V = [];
        W.forEach(w => {
            this._V[w] = [];
            Object.keys(V[w]).forEach(p => {
                this._V[w][p] = V[w][p];
            })
        })
    }

    public getNeighbours(w: State, agent: string): State[] {
        let r = this._R[w];
        if (r != undefined)
            r = r[agent];
            if (r != undefined)
                return r;
        return [];
    }

    public V(w: State, p: string): boolean {
        return this._V[w][p];
    }

    public addV(w: State, p: string, v: boolean): void {
        this._V[w][p] = v;
    }

    public transformAfterPublicAnnouncement(f: Formula): Model{
        let w: State[] = this._W.filter(value => f.evaluate(this, value));
        let r = [];
        w.forEach(w1 => {
            r[w1] = [];
            Object.keys(this._R[w1]).forEach(a => {
                r[w1][a] = this._R[w1][a].filter(value => w.indexOf(value) != -1)
            });
        });
        return new Model(w, r, this._baseV)
    }

    get W(): State[] {
        return this._W;
    }

    get baseV(): boolean[][] {
        return this._baseV;
    }
}