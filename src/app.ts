import {Formula} from "./formula";
import {Model, State} from "./model";

let edgeDiv: HTMLDivElement = <HTMLDivElement>document.getElementById("edgeInputs");
let modelDiv: HTMLDivElement = <HTMLDivElement>document.getElementById("modelInputs");
let stateDiv: HTMLDivElement = <HTMLDivElement>document.getElementById("statePicker");
let formulaInput: HTMLInputElement = <HTMLInputElement>document.getElementById("formulaInput");
let announcementInput: HTMLInputElement = <HTMLInputElement>document.getElementById("publicAnnouncement");
let br = document.createElement("br");
let W: State[]; // States
let A: string[]; // Agents
let models: Model[] = [];
let currentModel: number = 0;
let w: State;
const getEdgeLabel = (w1, a, w2) => `${w1} R${a} ${w2}`;

function getStateCheck(origin: State, disabled: boolean = false, M?: Model): HTMLDivElement {
    let d = document.createElement("tr");
    A.forEach(a => {
        let dr = document.createElement("td");
        let r =  M != null ? M.getNeighbours(origin, a) : [];
        W.forEach(w => {
            let label: HTMLLabelElement = document.createElement("label");
            label.htmlFor = getEdgeLabel(origin, a, w);
            label.innerText = getEdgeLabel(origin, a, w);
            let input: HTMLInputElement = document.createElement("input");
            input.id = getEdgeLabel(origin, a, w);
            input.type = "checkbox";
            input.disabled = disabled;
            input.checked = r.indexOf(w) != -1; // TODO ineficiente
            dr.appendChild(label);
            dr.appendChild(input);
        });
        d.appendChild(dr)
    });
    return d
}

function updateEdgeSelector(disabled: boolean = false, M?: Model) {
    edgeDiv.innerText = "";
    W.forEach(w => {
        edgeDiv.appendChild(getStateCheck(w, disabled, M));
    });
}

function updateModelSelector(disabled: boolean = false, M?: Model) {
    modelDiv.innerText = "";
    W.forEach(w => {
        let label: HTMLLabelElement = document.createElement("label");
        label.htmlFor = `v${w}`;
        label.innerText = w;
        let input: HTMLInputElement = document.createElement("input");
        input.id = `v${w}`;
        input.disabled = disabled;
        if (M != null){
            let first = true;
            Object.keys(M.baseV[w]).forEach(p => {
                if (M.baseV[w][p]){
                    if (first){
                        first = false;
                        input.value += p
                    } else
                        input.value += "," + p
                }
            })
        }
        modelDiv.appendChild(label);
        modelDiv.appendChild(input);
        modelDiv.appendChild(br);
    });
}

function updateStatePicker(disabled: boolean = false) {
    stateDiv.innerText = "";
    W.forEach(state => {
        let label: HTMLLabelElement = document.createElement("label");
        label.htmlFor = `s${state}`;
        label.innerText = state;
        let input: HTMLInputElement = document.createElement("input");
        input.id = `s${state}`;
        input.type = "radio";
        input.name = "state";
        input.value = state;
        input.checked = state == w;
        input.disabled = disabled;
        input.addEventListener("change", ev => {
            let input: HTMLInputElement = <HTMLInputElement>ev.target;
            w = input.value;
            document.getElementById('state').innerText = w;
        });
        stateDiv.appendChild(label);
        stateDiv.appendChild(input);
        stateDiv.appendChild(br);
    });

}

function updateForms(disabled: boolean = false, M?: Model) {
    updateEdgeSelector(disabled, M);
    updateModelSelector(disabled, M);
    updateStatePicker(disabled)
}

function getStates(): void {
    let input: HTMLInputElement = <HTMLInputElement>document.getElementById('states');
    W = input.value.split(',');
    w = W[0];
    document.getElementById('state').innerText = w;
    input = <HTMLInputElement>document.getElementById('agents');
    A = input.value.split(',');
    updateForms();
}

function getR(): State[][][] {
    let R = [];
    W.forEach(w => {
        R[w] = [];
        A.forEach(a => {
            R[w][a] = [];
            W.forEach(ww => {
                let input: HTMLInputElement = <HTMLInputElement>document.getElementById(getEdgeLabel(w, a, ww));
                if (input.checked) R[w][a].push(ww);
            });
        });
    });
    return R
}

function getV(): boolean[][] {
    let V = [];
    W.forEach(w => {
        V[w] = [];
        let input: HTMLInputElement = <HTMLInputElement>document.getElementById(`v${w}`);
        input.value.split(',').forEach(p => {
            V[w][p] = true;
        })
    });
    return V
}

function getCurrentModel(): Model{
    if (locked) {
        return models[currentModel]
    } else {
        let R = getR();
        let V = getV();
        return new Model(W, R, V);
    }
}

function validate(): void {
    let M = getCurrentModel();
    let formulaString = formulaInput.value;
    let formula = Formula.parse(formulaString);
    let result = formula.evaluate(M, w);
    if (result)
        document.getElementById("lastResult").style.color = "#15ef15";
    else
        document.getElementById("lastResult").style.color = "#ef1515";
    console.log("Resultado:");
    console.log(result);
    console.log(formula);
    console.log(M);
}

let locked: boolean = false;

function fillInputs(disabled: boolean, M: Model): void {
    W = M.W;
    let input: HTMLInputElement = <HTMLInputElement>document.getElementById('states');
    input.disabled = disabled;
    document.getElementById('state').innerText = w;
    input = <HTMLInputElement>document.getElementById('agents');
    input.disabled = disabled;
    updateForms(disabled, M)
}

function announce(): void {
    let M = getCurrentModel();
    let formulaString = announcementInput.value;
    let formula = Formula.parse(formulaString);
    if (!formula.evaluate(M, w)){
        alert("Anúncio Publico inválido")
    } else {
        let newM = M.transformAfterPublicAnnouncement(formula);
        fillInputs(locked, newM);
        if (locked){
            for (let i = ++currentModel; i < models.length; i++) {
                models.pop();
            }
            (<HTMLButtonElement>document.getElementById("undo")).disabled = false;
            (<HTMLButtonElement>document.getElementById("redo")).disabled = true;
            models[currentModel] = newM;
        }
        console.log(models)
    }
}

function lockModel(): void {
    let M = getCurrentModel();
    locked = !locked;
    if (locked){
        models = [];
        currentModel = 0;
        models[0] = M;
        fillInputs(true, M);
        (<HTMLButtonElement>document.getElementById("getStates")).disabled = true;
        document.getElementById("lockModel").innerText = "Desbloquear modelo"
    } else {
        models = [];
        fillInputs(false, M);
        // @ts-ignore
        (<HTMLButtonElement>document.getElementById("getStates")).disabled = false;
        document.getElementById("lockModel").innerText = "Bloquear modelo"
    }
}

function undo(){
   if (currentModel > 0){
       currentModel--;
       let M = getCurrentModel();
       fillInputs(locked, M);
       (<HTMLButtonElement>document.getElementById("redo")).disabled = false;
   }
    if (currentModel == 0) {
       (<HTMLButtonElement>document.getElementById("undo")).disabled = true;
   }
}

function redo(){
    if (currentModel < models.length-1){
        currentModel++;
        let M = getCurrentModel();
        fillInputs(locked, M);
        (<HTMLButtonElement>document.getElementById("undo")).disabled = false;
    }
    if (currentModel == models.length-1){
        (<HTMLButtonElement>document.getElementById("redo")).disabled = true;
    }
}

document.getElementById("getStates").addEventListener("click", getStates);
document.getElementById("validate").addEventListener("click", validate);
document.getElementById("announce").addEventListener("click", announce);
document.getElementById("lockModel").addEventListener("click", lockModel);
document.getElementById("undo").addEventListener("click", undo);
document.getElementById("redo").addEventListener("click", redo);
